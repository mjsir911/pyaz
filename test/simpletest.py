#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from pyaz.zoom import Connection, Query

import unittest

from contextlib import contextmanager

__appname__     = "simpletest"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


@contextmanager
def ztest():
    from subprocess import Popen, PIPE, DEVNULL
    from time import sleep
    try:
        ztest_server = Popen("yaz-ztest", stdout=DEVNULL, stderr=DEVNULL)
        sleep(0.01)  # wait for server to get running
        yield
    finally:
        ztest_server.kill()
        ztest_server.wait()


class TestConnection(unittest.TestCase):

    def with_ztest(func):
        from functools import wraps

        @wraps(func)
        def wrapper(*args, **kwargs):
            with ztest():
                return func(*args, **kwargs)
        return wrapper

    def test_failconnect(self):
        with self.assertRaises(Exception) as context:
            conn =Connection("localhost", 2)

            # Implementation defined, sometimes will not fail on initial
            # connection. See issue indexdata/yaz#46 on github

            query = Query("PQF", "@attr 1=7 9780596007126")
            conn.search(query)

    @with_ztest
    def test_connect(self):
        Connection("localhost", 9999)

    @with_ztest
    def test_attribute(self):
        conn = Connection("localhost", 9999)
        self.assertEqual(conn.host, "localhost")

        # test integer coersion
        self.assertEqual(conn.port, 9999)

        # test defaults
        self.assertEqual(conn.sru, None)
