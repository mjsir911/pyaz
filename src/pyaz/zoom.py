#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

"""
ZOOM is an acronym for 'Z39.50 Object-Orientation Model' and is an initiative
started by Mike Taylor (Mike is from the UK, which explains the peculiar name
of the model). The goal of ZOOM is to provide a common Z39.50 client API not
bound to a particular programming language or toolkit.
"""

import collections.abc
from abc import ABCMeta as _ABCMeta
from ._gen import zoom as _zoom
from ._helpers import _AttrCheck, dictSig as _dictSig, derivedMeta
from ._helpers import objectmethod, staticmethod
from logging import getLogger, basicConfig

_PyCStructType = type(_zoom.ctypes.Structure)

__appname__     = "zoom"
__author__      = "marco@sirabella.org"
__copyright__   = ""
__credits__     = ["Marco Sirabella <marco@sirabella.org>"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

log = getLogger(__name__)
basicConfig()


class Query(_zoom.query._type_):
    def __new__(cls, *args, **kwargs):
        log.debug('return _zoom.query_create()')
        return cls.from_buffer(_zoom.query_create().contents)

    def __init__(self, typ, query):
        """
        type can one of:
        CQL, PQF
        """
        self.type = typ.upper()
        self.query = query

        from functools import partial
        dispatch = {
            "CQL": partial(_zoom.query_cql, self),
            "PQF": partial(_zoom.query_prefix, self)
        }

        log.debug('dispatch[self.type](query.encode())')
        dispatch[self.type](query.encode())

    __del__ = objectmethod(_zoom.query_destroy)


class Record(_zoom.record._type_):
    def _errable(f):
        from functools import wraps

        @wraps(f)
        def wrapper(self, *args, **kwargs):
            ret = f(self, *args, **kwargs)

            from ctypes import POINTER, c_char_p
            errmsg  = POINTER(c_char_p)()
            addinfo = POINTER(c_char_p)()
            diagset = POINTER(c_char_p)()

            log.debug('errcode = _zoom.record_error(self, errmsg, addinfo, diagset)')
            errcode = _zoom.record_error(self, errmsg, addinfo, diagset)
            if errcode != 0:
                raise Exception(errcode, errms, addinfo, diagset)
            return ret
        return wrapper

    def __new__(cls, ptr):
        if not ptr:
            raise ValueError("Record is unavailable")
        return cls.from_buffer(_zoom.record_clone(ptr).contents)

    def __init__(self, ptr):  # why segault if no init, probs subclassing mess
        pass

    def __eq__(self, other):
        return self.get() == other.get()

    @_errable
    def get(self, fmt="render; charset=marc8,iso-8859-1"):
        from ctypes import POINTER, c_int
        nothing = POINTER(c_int)()
        log.debug('ret = _zoom.record_get(self, fmt.encode(), nothing)')
        ret = _zoom.record_get(self, fmt.encode(), nothing)
        return ret

    __del__ = objectmethod(_zoom.record_destroy)


class ResultSet(_AttrCheck,
                _zoom.resultset._type_,
                collections.abc.Set,
                metaclass=derivedMeta(_PyCStructType, _ABCMeta)
                ):

    def _options_set(self, key, value):
        from ctypes import c_char_p
        if value is None:
            value = c_char_p(value)
        elif isinstance(value, str):
            value = c_char_p(value.encode())
        else:
            value = c_char_p(str(value).encode())
        log.debug('return _zoom.resultset_option_set(self, key.encode(), value)')
        return _zoom.resultset_option_set(self, key.encode(), value)

    _options_get = lambda s, k: _zoom.resultset_option_get(s, k.encode())
    __options__ = {"start": 0,
                   "count": 0,
                   "presentChunk": 0,
                   "elementSetName": None,
                   "preferredRecordSyntax": None,
                   "schema": None,
                   "setname": "default",
                   "rpnCharset": None}

    def __new__(cls, ptr, *args, **kwargs):
        log.debug('resultSet __new__')
        return cls.from_buffer(ptr.contents)

    @_dictSig(__options__)
    def __init__(self, ptr, **options):
        super().__init__(options)

    # For set
    def __contains__(self, thing):
        return thing in list(self)

    def __iter__(self):
        for i in range(0, len(self)):
            log.debug('yield Record(_zoom.resultset_record(self, i))')
            yield Record(_zoom.resultset_record(self, i))

    def __len__(self):
        log.debug('return _zoom.resultset_size(self)')
        return _zoom.resultset_size(self)

    __del__ = objectmethod(_zoom.resultset_destroy)


class Options(_zoom.struct_options_p,
              collections.abc.MutableMapping,
              metaclass=type('', (_PyCStructType, _ABCMeta), {})):

    def __new__(cls):
        return cls.from_buffer(_zoom.options_create().contents)

    def __setitem__(self, key, val):
        if val is None:
            log.debug('_zoom.options_set(self, key, _zoom.ctypes.c_char_p(0))')
            _zoom.options_set(self, key, _zoom.ctypes.c_char_p(0))
        elif isinstance(val, str):
            log.debug('_zoom.options_set(self, key, val.encode())')
            _zoom.options_set(self, key, val.encode())
        else:
            log.debug('_zoom.options_set_int(self, key, int(val))')
            _zoom.options_set_int(self, key, int(val))

    def __getitem__(self, key):
        log.debug('return _zoom.options_get(self, key).decode()')
        return _zoom.options_get(self, key).decode()

    def __delitem__(self, key):
        raise

    def __iter__(self):
        raise

    def __len__(self):
        raise

    __del__ = objectmethod(_zoom.options_destroy)


class Connection(_AttrCheck, _zoom.struct_connection_p):
    __options__ = dict(host=None,
                       port=0,
                       implementationName="pyaz",
                       user=None,
                       group=None,
                       password=None,
                       authenticationMode="basic",
                       proxy=None,
                       clientIP=None,
                       timeout=30,
                       async_=False,
                       maximumRecordSize=64 * 1024 * 1024,  # 1 MB
                       preferredMessageSize=64 * 1024 * 1024,  # 1 MB
                       lang=None,
                       charset=None,
                       serverImplementationId=None,
                       targetImplementationName=None,
                       serverImplementationVersion=None,
                       databaseName=None,
                       piggyback=True,
                       smallSetUpperBound=0,
                       largeSetLowerBound=1,
                       mediumSetPresentNumber=0,
                       smallSetElementSetName=None,
                       mediumSetElementSetName=None,
                       init_opt_search=None,
                       init_opt_present=None,
                       init_opt_delSet=None,
                       # Etc?
                       sru=None,  # Ok ok, so the indexdata documentation on
                                  # first glance looks like the default value
                                  # for sru is 'soap', but from much
                                  # delibration (github/indexdata/yaz#46/47) it
                                  # looks like the real default is "z39.50
                                  # mode" which is sru as null pointer
                       sru_version="1.2",
                       extraArgs=None,
                       facets=None,
                       apdulog=False,
                       saveAPDU=False,
                       APDU=None,
                       memcached=None,
                       redis=None,
                       )

    @staticmethod
    def _error(err):
        # TODO: one day `-k m` will work in clang2py and macros will be
        # available defining these magic numbers

        # Switches would be nice right about now
        from functools import partial
        return {
            236:   ConnectionRefusedError,
            10000: ConnectionError,
            10007: TimeoutError,
            10010: SyntaxError,
        }.get(err, partial(Exception, err))(_zoom.diag_str(err).decode())

    @staticmethod
    def _errable(f):
        from functools import wraps

        @wraps(f)
        def wrapper(self, *args, **kwargs):
            log.debug('_errable f()')
            ret = f(self, *args, **kwargs)
            log.debug('_errable f() ~')

            log.debug('errcode = _zoom.connection_errcode(self)')
            errcode = _zoom.connection_errcode(self)
            log.debug('errcode = _zoom.connection_errcode(self) end')
            if errcode != 0:
                raise self._error(errcode)
            return ret
        return wrapper

    def __new__(cls, *args, **kwargs):
        _options = Options()
        log.debug('self = _zoom.connection_create(_options)')
        self = cls.from_buffer(_zoom.connection_create(_options).contents)
        self.__closed = False  # should be as close to instantiation as possible
        self._options = _options
        return self

    @_errable
    @_dictSig(__options__)
    def __init__(self, host, port=0, **options):
        super().__init__({"host": host, "port": port, **options})
        log.debug('_zoom.connection_connect(self, host.encode(), port)')
        _zoom.connection_connect(self, host.encode(), port)

    def _options_set(self, key, val):
        self._options[key.strip("_").encode()] = val

    def _options_get(self, key):
        return self._options[key.encode()]

    def __hash__(self):
        return hash(id(self))

    @_dictSig(ResultSet.__options__)
    def search(self, query, **kwargs):
        log.debug('return ResultSet(self._errable(_zoom.connection_search)(self, query),')
        return ResultSet(self._errable(_zoom.connection_search)(self, query),
                         **kwargs)

    def close(self):
        if not self.__closed:
            self.__closed = True
            _zoom.connection_destroy(self)

    __del__ = close
