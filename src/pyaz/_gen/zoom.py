# -*- coding: utf-8 -*-
#
# TARGET arch is: []
# WORD_SIZE is: 8
# POINTER_SIZE is: 8
# LONGDOUBLE_SIZE is: 16
#
# this file generated with:
"""
clang2py --debug \
         --show-definition-location \
         --include-library yaz /usr/include/yaz/zoom.h \
| sed -e 's|POINTER_T(ctypes.c_char)|ctypes.c_char_p|g' \
      -e 's#\([^\.]\|^\)ZOOM_#\1#g' > zoom.py
"""
import ctypes


# if local wordsize is same as target, keep ctypes pointer function.
if ctypes.sizeof(ctypes.c_void_p) == 8:
    POINTER_T = ctypes.POINTER
else:
    # required to access _ctypes
    import _ctypes
    # Emulate a pointer class using the approriate c_int32/c_int64 type
    # The new class should have :
    # ['__module__', 'from_param', '_type_', '__dict__', '__weakref__', '__doc__']
    # but the class should be submitted to a unique instance for each base type
    # to that if A == B, POINTER_T(A) == POINTER_T(B)
    ctypes._pointer_t_type_cache = {}
    def POINTER_T(pointee):
        # a pointer should have the same length as LONG
        fake_ptr_base_type = ctypes.c_uint64
        # specific case for c_void_p
        if pointee is None: # VOID pointer type. c_void_p.
            pointee = type(None) # ctypes.c_void_p # ctypes.c_ulong
            clsname = 'c_void'
        else:
            clsname = pointee.__name__
        if clsname in ctypes._pointer_t_type_cache:
            return ctypes._pointer_t_type_cache[clsname]
        # make template
        class _T(_ctypes._SimpleCData,):
            _type_ = 'L'
            _subtype_ = pointee
            def _sub_addr_(self):
                return self.value
            def __repr__(self):
                return '%s(%d)'%(clsname, self.value)
            def contents(self):
                raise TypeError('This is not a ctypes pointer.')
            def __init__(self, **args):
                raise TypeError('This is not a ctypes pointer. It is not instanciable.')
        _class = type('LP_%d_%s'%(8, clsname), (_T,),{})
        ctypes._pointer_t_type_cache[clsname] = _class
        return _class

c_int128 = ctypes.c_ubyte*16
c_uint128 = c_int128
void = None
if ctypes.sizeof(ctypes.c_longdouble) == 16:
    c_long_double_t = ctypes.c_longdouble
else:
    c_long_double_t = ctypes.c_ubyte*16

_libraries = {}
for lib in ["libyaz.so", "libyaz.so.5", "libyaz.so.3", "libyaz.dylib"]:
    try:
        _libraries['libyaz.so'] = ctypes.CDLL(lib)
    except OSError as e:
        continue
    else:
        break


# /usr/include/yaz/zoom.h:50
# /usr/include/yaz/zoom.h:50
class struct_options_p(ctypes.Structure):
    pass

options = POINTER_T(struct_options_p)
# /usr/include/yaz/zoom.h:50
# /usr/include/yaz/zoom.h:51
# /usr/include/yaz/zoom.h:51
class struct_query_p(ctypes.Structure):
    pass

query = POINTER_T(struct_query_p)
# /usr/include/yaz/zoom.h:51
# /usr/include/yaz/zoom.h:52
class struct_connection_p(ctypes.Structure):
    pass

# /usr/include/yaz/zoom.h:52
# /usr/include/yaz/zoom.h:52
connection = POINTER_T(struct_connection_p)
# /usr/include/yaz/zoom.h:53
class struct_resultset_p(ctypes.Structure):
    pass

# /usr/include/yaz/zoom.h:53
# /usr/include/yaz/zoom.h:53
resultset = POINTER_T(struct_resultset_p)
# /usr/include/yaz/zoom.h:54
class struct_record_p(ctypes.Structure):
    pass

# /usr/include/yaz/zoom.h:54
# /usr/include/yaz/zoom.h:54
record = POINTER_T(struct_record_p)
# /usr/include/yaz/zoom.h:55
class struct_scanset_p(ctypes.Structure):
    pass

# /usr/include/yaz/zoom.h:55
# /usr/include/yaz/zoom.h:55
scanset = POINTER_T(struct_scanset_p)
# /usr/include/yaz/zoom.h:56
class struct_package_p(ctypes.Structure):
    pass

# /usr/include/yaz/zoom.h:56
# /usr/include/yaz/zoom.h:56
package = POINTER_T(struct_package_p)
# /usr/include/yaz/zoom.h:58
options_callback = POINTER_T(ctypes.CFUNCTYPE(ctypes.c_char_p, POINTER_T(None), ctypes.c_char_p))
# /usr/include/yaz/zoom.h:66
# /usr/include/yaz/zoom.h 66
connection_new = _libraries['libyaz.so'].ZOOM_connection_new
connection_new.restype = connection
connection_new.argtypes = [ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:70
# /usr/include/yaz/zoom.h 70
connection_create = _libraries['libyaz.so'].ZOOM_connection_create
connection_create.restype = connection
connection_create.argtypes = [options]
# /usr/include/yaz/zoom.h:74
# /usr/include/yaz/zoom.h 74
connection_connect = _libraries['libyaz.so'].ZOOM_connection_connect
connection_connect.restype = None
connection_connect.argtypes = [connection, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:79
# /usr/include/yaz/zoom.h 79
connection_destroy = _libraries['libyaz.so'].ZOOM_connection_destroy
connection_destroy.restype = None
connection_destroy.argtypes = [connection]
# /usr/include/yaz/zoom.h:83
# /usr/include/yaz/zoom.h 83
connection_option_get = _libraries['libyaz.so'].ZOOM_connection_option_get
connection_option_get.restype = ctypes.c_char_p
connection_option_get.argtypes = [connection, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:86
# /usr/include/yaz/zoom.h 86
connection_option_getl = _libraries['libyaz.so'].ZOOM_connection_option_getl
connection_option_getl.restype = ctypes.c_char_p
connection_option_getl.argtypes = [connection, ctypes.c_char_p, POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:89
# /usr/include/yaz/zoom.h 89
connection_option_set = _libraries['libyaz.so'].ZOOM_connection_option_set
connection_option_set.restype = None
connection_option_set.argtypes = [connection, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:93
# /usr/include/yaz/zoom.h 93
connection_option_setl = _libraries['libyaz.so'].ZOOM_connection_option_setl
connection_option_setl.restype = None
connection_option_setl.argtypes = [connection, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:100
# /usr/include/yaz/zoom.h 100
connection_error = _libraries['libyaz.so'].ZOOM_connection_error
connection_error.restype = ctypes.c_int32
connection_error.argtypes = [connection, POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_char_p)]
# /usr/include/yaz/zoom.h:104
# /usr/include/yaz/zoom.h 104
connection_error_x = _libraries['libyaz.so'].ZOOM_connection_error_x
connection_error_x.restype = ctypes.c_int32
connection_error_x.argtypes = [connection, POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_char_p)]
# /usr/include/yaz/zoom.h:109
# /usr/include/yaz/zoom.h 109
connection_errcode = _libraries['libyaz.so'].ZOOM_connection_errcode
connection_errcode.restype = ctypes.c_int32
connection_errcode.argtypes = [connection]
# /usr/include/yaz/zoom.h:112
# /usr/include/yaz/zoom.h 112
connection_errmsg = _libraries['libyaz.so'].ZOOM_connection_errmsg
connection_errmsg.restype = ctypes.c_char_p
connection_errmsg.argtypes = [connection]
# /usr/include/yaz/zoom.h:115
# /usr/include/yaz/zoom.h 115
connection_addinfo = _libraries['libyaz.so'].ZOOM_connection_addinfo
connection_addinfo.restype = ctypes.c_char_p
connection_addinfo.argtypes = [connection]
# /usr/include/yaz/zoom.h:118
# /usr/include/yaz/zoom.h 118
connection_diagset = _libraries['libyaz.so'].ZOOM_connection_diagset
connection_diagset.restype = ctypes.c_char_p
connection_diagset.argtypes = [connection]
# /usr/include/yaz/zoom.h:121
# /usr/include/yaz/zoom.h 121
diag_str = _libraries['libyaz.so'].ZOOM_diag_str
diag_str.restype = ctypes.c_char_p
diag_str.argtypes = [ctypes.c_int32]
# /usr/include/yaz/zoom.h:141
# /usr/include/yaz/zoom.h 141
connection_last_event = _libraries['libyaz.so'].ZOOM_connection_last_event
connection_last_event.restype = ctypes.c_int32
connection_last_event.argtypes = [connection]
# /usr/include/yaz/zoom.h:161
# /usr/include/yaz/zoom.h 161
connection_search = _libraries['libyaz.so'].ZOOM_connection_search
connection_search.restype = resultset
connection_search.argtypes = [connection, query]
# /usr/include/yaz/zoom.h:164
# /usr/include/yaz/zoom.h 164
connection_search_pqf = _libraries['libyaz.so'].ZOOM_connection_search_pqf
connection_search_pqf.restype = resultset
connection_search_pqf.argtypes = [connection, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:168
# /usr/include/yaz/zoom.h 168
resultset_destroy = _libraries['libyaz.so'].ZOOM_resultset_destroy
resultset_destroy.restype = None
resultset_destroy.argtypes = [resultset]
# /usr/include/yaz/zoom.h:172
# /usr/include/yaz/zoom.h 172
resultset_option_get = _libraries['libyaz.so'].ZOOM_resultset_option_get
resultset_option_get.restype = ctypes.c_char_p
resultset_option_get.argtypes = [resultset, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:174
# /usr/include/yaz/zoom.h 174
resultset_option_set = _libraries['libyaz.so'].ZOOM_resultset_option_set
resultset_option_set.restype = None
resultset_option_set.argtypes = [resultset, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:178
# /usr/include/yaz/zoom.h 178
resultset_size = _libraries['libyaz.so'].ZOOM_resultset_size
resultset_size.restype = ctypes.c_int32
resultset_size.argtypes = [resultset]
# /usr/include/yaz/zoom.h:182
# /usr/include/yaz/zoom.h:54
# /usr/include/yaz/zoom.h:54
# /usr/include/yaz/zoom.h 182
resultset_records = _libraries['libyaz.so'].ZOOM_resultset_records
resultset_records.restype = None
resultset_records.argtypes = [resultset, POINTER_T(POINTER_T(struct_record_p)), ctypes.c_int32, ctypes.c_int32]
# /usr/include/yaz/zoom.h:187
# /usr/include/yaz/zoom.h 187
resultset_record = _libraries['libyaz.so'].ZOOM_resultset_record
resultset_record.restype = record
resultset_record.argtypes = [resultset, ctypes.c_int32]
# /usr/include/yaz/zoom.h:191
# /usr/include/yaz/zoom.h 191
resultset_record_immediate = _libraries['libyaz.so'].ZOOM_resultset_record_immediate
resultset_record_immediate.restype = record
resultset_record_immediate.argtypes = [resultset, ctypes.c_int32]
# /usr/include/yaz/zoom.h:195
# /usr/include/yaz/zoom.h 195
resultset_cache_reset = _libraries['libyaz.so'].ZOOM_resultset_cache_reset
resultset_cache_reset.restype = None
resultset_cache_reset.argtypes = [resultset]
# /usr/include/yaz/zoom.h:202
# /usr/include/yaz/zoom.h 202
record_get = _libraries['libyaz.so'].ZOOM_record_get
record_get.restype = ctypes.c_char_p
record_get.argtypes = [record, ctypes.c_char_p, POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:206
# /usr/include/yaz/zoom.h 206
record_destroy = _libraries['libyaz.so'].ZOOM_record_destroy
record_destroy.restype = None
record_destroy.argtypes = [record]
# /usr/include/yaz/zoom.h:210
# /usr/include/yaz/zoom.h 210
record_clone = _libraries['libyaz.so'].ZOOM_record_clone
record_clone.restype = record
record_clone.argtypes = [record]
# /usr/include/yaz/zoom.h:214
# /usr/include/yaz/zoom.h 214
record_error = _libraries['libyaz.so'].ZOOM_record_error
record_error.restype = ctypes.c_int32
record_error.argtypes = [record, POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_char_p)]
# /usr/include/yaz/zoom.h:222
# /usr/include/yaz/zoom.h 222
query_create = _libraries['libyaz.so'].ZOOM_query_create
query_create.restype = query
query_create.argtypes = []
# /usr/include/yaz/zoom.h:225
# /usr/include/yaz/zoom.h 225
query_destroy = _libraries['libyaz.so'].ZOOM_query_destroy
query_destroy.restype = None
query_destroy.argtypes = [query]
# /usr/include/yaz/zoom.h:228
# /usr/include/yaz/zoom.h 228
query_cql = _libraries['libyaz.so'].ZOOM_query_cql
query_cql.restype = ctypes.c_int32
query_cql.argtypes = [query, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:231
# /usr/include/yaz/zoom.h 231
query_cql2rpn = _libraries['libyaz.so'].ZOOM_query_cql2rpn
query_cql2rpn.restype = ctypes.c_int32
query_cql2rpn.argtypes = [query, ctypes.c_char_p, connection]
# /usr/include/yaz/zoom.h:234
# /usr/include/yaz/zoom.h 234
query_ccl2rpn = _libraries['libyaz.so'].ZOOM_query_ccl2rpn
query_ccl2rpn.restype = ctypes.c_int32
query_ccl2rpn.argtypes = [query, ctypes.c_char_p, ctypes.c_char_p, POINTER_T(ctypes.c_int32), POINTER_T(ctypes.c_char_p), POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:239
# /usr/include/yaz/zoom.h 239
query_prefix = _libraries['libyaz.so'].ZOOM_query_prefix
query_prefix.restype = ctypes.c_int32
query_prefix.argtypes = [query, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:242
# /usr/include/yaz/zoom.h 242
query_sortby = _libraries['libyaz.so'].ZOOM_query_sortby
query_sortby.restype = ctypes.c_int32
query_sortby.argtypes = [query, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:247
# /usr/include/yaz/zoom.h 247
connection_scan = _libraries['libyaz.so'].ZOOM_connection_scan
connection_scan.restype = scanset
connection_scan.argtypes = [connection, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:250
# /usr/include/yaz/zoom.h 250
connection_scan1 = _libraries['libyaz.so'].ZOOM_connection_scan1
connection_scan1.restype = scanset
connection_scan1.argtypes = [connection, query]
# /usr/include/yaz/zoom.h:253
# /usr/include/yaz/zoom.h 253
scanset_term = _libraries['libyaz.so'].ZOOM_scanset_term
scanset_term.restype = ctypes.c_char_p
scanset_term.argtypes = [scanset, ctypes.c_int32, POINTER_T(ctypes.c_int32), POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:257
# /usr/include/yaz/zoom.h 257
scanset_display_term = _libraries['libyaz.so'].ZOOM_scanset_display_term
scanset_display_term.restype = ctypes.c_char_p
scanset_display_term.argtypes = [scanset, ctypes.c_int32, POINTER_T(ctypes.c_int32), POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:261
# /usr/include/yaz/zoom.h 261
scanset_size = _libraries['libyaz.so'].ZOOM_scanset_size
scanset_size.restype = ctypes.c_int32
scanset_size.argtypes = [scanset]
# /usr/include/yaz/zoom.h:264
# /usr/include/yaz/zoom.h 264
scanset_destroy = _libraries['libyaz.so'].ZOOM_scanset_destroy
scanset_destroy.restype = None
scanset_destroy.argtypes = [scanset]
# /usr/include/yaz/zoom.h:267
# /usr/include/yaz/zoom.h 267
scanset_option_get = _libraries['libyaz.so'].ZOOM_scanset_option_get
scanset_option_get.restype = ctypes.c_char_p
scanset_option_get.argtypes = [scanset, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:270
# /usr/include/yaz/zoom.h 270
scanset_option_set = _libraries['libyaz.so'].ZOOM_scanset_option_set
scanset_option_set.restype = None
scanset_option_set.argtypes = [scanset, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:276
# /usr/include/yaz/zoom.h 276
connection_package = _libraries['libyaz.so'].ZOOM_connection_package
connection_package.restype = package
connection_package.argtypes = [connection, options]
# /usr/include/yaz/zoom.h:279
# /usr/include/yaz/zoom.h 279
package_destroy = _libraries['libyaz.so'].ZOOM_package_destroy
package_destroy.restype = None
package_destroy.argtypes = [package]
# /usr/include/yaz/zoom.h:282
# /usr/include/yaz/zoom.h 282
package_send = _libraries['libyaz.so'].ZOOM_package_send
package_send.restype = None
package_send.argtypes = [package, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:285
# /usr/include/yaz/zoom.h 285
package_option_get = _libraries['libyaz.so'].ZOOM_package_option_get
package_option_get.restype = ctypes.c_char_p
package_option_get.argtypes = [package, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:288
# /usr/include/yaz/zoom.h 288
package_option_getl = _libraries['libyaz.so'].ZOOM_package_option_getl
package_option_getl.restype = ctypes.c_char_p
package_option_getl.argtypes = [package, ctypes.c_char_p, POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:291
# /usr/include/yaz/zoom.h 291
package_option_set = _libraries['libyaz.so'].ZOOM_package_option_set
package_option_set.restype = None
package_option_set.argtypes = [package, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:295
# /usr/include/yaz/zoom.h 295
package_option_setl = _libraries['libyaz.so'].ZOOM_package_option_setl
package_option_setl.restype = None
package_option_setl.argtypes = [package, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:301
# /usr/include/yaz/zoom.h 301
resultset_sort = _libraries['libyaz.so'].ZOOM_resultset_sort
resultset_sort.restype = None
resultset_sort.argtypes = [resultset, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:304
# /usr/include/yaz/zoom.h 304
resultset_sort1 = _libraries['libyaz.so'].ZOOM_resultset_sort1
resultset_sort1.restype = ctypes.c_int32
resultset_sort1.argtypes = [resultset, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:311
# /usr/include/yaz/zoom.h 311
options_set_callback = _libraries['libyaz.so'].ZOOM_options_set_callback
options_set_callback.restype = options_callback
options_set_callback.argtypes = [options, options_callback, POINTER_T(None)]
# /usr/include/yaz/zoom.h:314
# /usr/include/yaz/zoom.h 314
options_create = _libraries['libyaz.so'].ZOOM_options_create
options_create.restype = options
options_create.argtypes = []
# /usr/include/yaz/zoom.h:317
# /usr/include/yaz/zoom.h 317
options_create_with_parent = _libraries['libyaz.so'].ZOOM_options_create_with_parent
options_create_with_parent.restype = options
options_create_with_parent.argtypes = [options]
# /usr/include/yaz/zoom.h:320
# /usr/include/yaz/zoom.h 320
options_create_with_parent2 = _libraries['libyaz.so'].ZOOM_options_create_with_parent2
options_create_with_parent2.restype = options
options_create_with_parent2.argtypes = [options, options]
# /usr/include/yaz/zoom.h:324
# /usr/include/yaz/zoom.h 324
options_dup = _libraries['libyaz.so'].ZOOM_options_dup
options_dup.restype = options
options_dup.argtypes = [options]
# /usr/include/yaz/zoom.h:327
# /usr/include/yaz/zoom.h 327
options_get = _libraries['libyaz.so'].ZOOM_options_get
options_get.restype = ctypes.c_char_p
options_get.argtypes = [options, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:330
# /usr/include/yaz/zoom.h 330
options_getl = _libraries['libyaz.so'].ZOOM_options_getl
options_getl.restype = ctypes.c_char_p
options_getl.argtypes = [options, ctypes.c_char_p, POINTER_T(ctypes.c_int32)]
# /usr/include/yaz/zoom.h:333
# /usr/include/yaz/zoom.h 333
options_set = _libraries['libyaz.so'].ZOOM_options_set
options_set.restype = None
options_set.argtypes = [options, ctypes.c_char_p, ctypes.c_char_p]
# /usr/include/yaz/zoom.h:336
# /usr/include/yaz/zoom.h 336
options_setl = _libraries['libyaz.so'].ZOOM_options_setl
options_setl.restype = None
options_setl.argtypes = [options, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:340
# /usr/include/yaz/zoom.h 340
options_destroy = _libraries['libyaz.so'].ZOOM_options_destroy
options_destroy.restype = None
options_destroy.argtypes = [options]
# /usr/include/yaz/zoom.h:343
# /usr/include/yaz/zoom.h 343
options_get_bool = _libraries['libyaz.so'].ZOOM_options_get_bool
options_get_bool.restype = ctypes.c_int32
options_get_bool.argtypes = [options, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:346
# /usr/include/yaz/zoom.h 346
options_get_int = _libraries['libyaz.so'].ZOOM_options_get_int
options_get_int.restype = ctypes.c_int32
options_get_int.argtypes = [options, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:349
# /usr/include/yaz/zoom.h 349
options_set_int = _libraries['libyaz.so'].ZOOM_options_set_int
options_set_int.restype = None
options_set_int.argtypes = [options, ctypes.c_char_p, ctypes.c_int32]
# /usr/include/yaz/zoom.h:370
# /usr/include/yaz/zoom.h:52
# /usr/include/yaz/zoom.h:52
# /usr/include/yaz/zoom.h 370
event = _libraries['libyaz.so'].ZOOM_event
event.restype = ctypes.c_int32
event.argtypes = [ctypes.c_int32, POINTER_T(POINTER_T(struct_connection_p))]
# /usr/include/yaz/zoom.h:379
# /usr/include/yaz/zoom.h 379
connection_is_idle = _libraries['libyaz.so'].ZOOM_connection_is_idle
connection_is_idle.restype = ctypes.c_int32
connection_is_idle.argtypes = [connection]
# /usr/include/yaz/zoom.h:396
# /usr/include/yaz/zoom.h:52
# /usr/include/yaz/zoom.h:52
# /usr/include/yaz/zoom.h 396
event_nonblock = _libraries['libyaz.so'].ZOOM_event_nonblock
event_nonblock.restype = ctypes.c_int32
event_nonblock.argtypes = [ctypes.c_int32, POINTER_T(POINTER_T(struct_connection_p))]
# /usr/include/yaz/zoom.h:413
# /usr/include/yaz/zoom.h 413
connection_process = _libraries['libyaz.so'].ZOOM_connection_process
connection_process.restype = ctypes.c_int32
connection_process.argtypes = [connection]
# /usr/include/yaz/zoom.h:425
# /usr/include/yaz/zoom.h 425
connection_get_socket = _libraries['libyaz.so'].ZOOM_connection_get_socket
connection_get_socket.restype = ctypes.c_int32
connection_get_socket.argtypes = [connection]
# /usr/include/yaz/zoom.h:436
# /usr/include/yaz/zoom.h 436
connection_get_mask = _libraries['libyaz.so'].ZOOM_connection_get_mask
connection_get_mask.restype = ctypes.c_int32
connection_get_mask.argtypes = [connection]
# /usr/include/yaz/zoom.h:441
# /usr/include/yaz/zoom.h 441
connection_set_mask = _libraries['libyaz.so'].ZOOM_connection_set_mask
connection_set_mask.restype = ctypes.c_int32
connection_set_mask.argtypes = [connection, ctypes.c_int32]
# /usr/include/yaz/zoom.h:452
# /usr/include/yaz/zoom.h 452
connection_get_timeout = _libraries['libyaz.so'].ZOOM_connection_get_timeout
connection_get_timeout.restype = ctypes.c_int32
connection_get_timeout.argtypes = [connection]
# /usr/include/yaz/zoom.h:464
# /usr/include/yaz/zoom.h 464
connection_fire_event_timeout = _libraries['libyaz.so'].ZOOM_connection_fire_event_timeout
connection_fire_event_timeout.restype = ctypes.c_int32
connection_fire_event_timeout.argtypes = [connection]
# /usr/include/yaz/zoom.h:474
# /usr/include/yaz/zoom.h 474
connection_fire_event_socket = _libraries['libyaz.so'].ZOOM_connection_fire_event_socket
connection_fire_event_socket.restype = ctypes.c_int32
connection_fire_event_socket.argtypes = [connection, ctypes.c_int32]
# /usr/include/yaz/zoom.h:487
# /usr/include/yaz/zoom.h 487
connection_peek_event = _libraries['libyaz.so'].ZOOM_connection_peek_event
connection_peek_event.restype = ctypes.c_int32
connection_peek_event.argtypes = [connection]
# /usr/include/yaz/zoom.h:490
# /usr/include/yaz/zoom.h 490
get_event_str = _libraries['libyaz.so'].ZOOM_get_event_str
get_event_str.restype = ctypes.c_char_p
get_event_str.argtypes = [ctypes.c_int32]
__all__ = \
    ['connection', 'connection_addinfo',
    'connection_connect', 'connection_create',
    'connection_destroy', 'connection_diagset',
    'connection_errcode', 'connection_errmsg',
    'connection_error', 'connection_error_x',
    'connection_fire_event_socket',
    'connection_fire_event_timeout', 'connection_get_mask',
    'connection_get_socket', 'connection_get_timeout',
    'connection_is_idle', 'connection_last_event',
    'connection_new', 'connection_option_get',
    'connection_option_getl', 'connection_option_set',
    'connection_option_setl', 'connection_package',
    'connection_peek_event', 'connection_process',
    'connection_scan', 'connection_scan1',
    'connection_search', 'connection_search_pqf',
    'connection_set_mask', 'diag_str', 'event',
    'event_nonblock', 'get_event_str', 'options',
    'options_callback', 'options_create',
    'options_create_with_parent',
    'options_create_with_parent2', 'options_destroy',
    'options_dup', 'options_get', 'options_get_bool',
    'options_get_int', 'options_getl', 'options_set',
    'options_set_callback', 'options_set_int',
    'options_setl', 'package', 'package_destroy',
    'package_option_get', 'package_option_getl',
    'package_option_set', 'package_option_setl',
    'package_send', 'query', 'query_ccl2rpn',
    'query_cql', 'query_cql2rpn', 'query_create',
    'query_destroy', 'query_prefix', 'query_sortby',
    'record', 'record_clone', 'record_destroy',
    'record_error', 'record_get', 'resultset',
    'resultset_cache_reset', 'resultset_destroy',
    'resultset_option_get', 'resultset_option_set',
    'resultset_record', 'resultset_record_immediate',
    'resultset_records', 'resultset_size',
    'resultset_sort', 'resultset_sort1', 'scanset',
    'scanset_destroy', 'scanset_display_term',
    'scanset_option_get', 'scanset_option_set',
    'scanset_size', 'scanset_term',
    'struct_connection_p', 'struct_options_p',
    'struct_package_p', 'struct_query_p',
    'struct_record_p', 'struct_resultset_p',
    'struct_scanset_p']
