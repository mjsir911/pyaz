#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

__appname__     = "_helpers"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

from functools import partial


class _AttrCheck():
    __options__ = {}
    def __init__(self, options_dict):
        for key, val in {
                **self.__options__,
                **options_dict
        }.items():
            setattr(self, key, val)

    def __getattr__(self, key):
        if key in self.__options__:
            return self._options_get(key)
        return super().__getattribute__(key)

    def __setattr__(self, key, val):
        if key in self.__options__:
            return self._options_set(key, val)
        #super().__setattribute__(self, key, val)
        if key.startswith("_"):
            return super().__setattr__(key, val)
        raise Exception(key)

    def __dir__(self):
        return super().__dir__() + list(self.__options__)


def validateSig(f):
    from functools import wraps

    @wraps(f)
    def wrapper(*args, **kwargs):
        a = f.__signature__.bind(*args, **kwargs)
        a.apply_defaults()
        return f(*a.args, **a.kwargs)
    return wrapper


@partial(partial, partial)  # amazing
def setSig(s, f):
    f.__signature__ = s
    return f


@partial(partial, partial)  # amazing
def dictSig(d, func):
    """
    this is just replacing the variable keyword arguments with a dictionary of
    arg: default values
    """
    from inspect import Signature, Parameter
    myparams = {Parameter(k, Parameter.KEYWORD_ONLY, default=v) for k, v in d.items()}
    funcparams = list(Signature.from_function(func).parameters.values())

    # take out options dict, this is what we're replacing
    funcparams = [p for p in funcparams if p.kind != Parameter.VAR_KEYWORD]

    funcparamnames = {p.name for p in funcparams}

    # don't duplicate args
    myparams = {param for param in myparams if param.name not in funcparamnames}

    from functools import wraps

    @validateSig
    @setSig(Signature(funcparams + list(myparams)))
    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


def derivedMeta(*metaclasses):
    return type('', metaclasses, {})


class objectmethod():
    def __init__(self, func):
        self.__func__ = func

    def __get__(self, slf, cls):
        from functools import partial
        return partial(self, slf)

    def __call__(self, *args, **kwargs):
        return self.__func__(*args, **kwargs)


class classmethod(classmethod):
    """
    just classmethod but implement __call__ so I can call outside
    """
    def __call__(self, *args, **kwargs):
        return self.__func__(*args, **kwargs)


class staticmethod(staticmethod):
    """
    just staticmethod but implement __call__ so I can call outside
    """
    def __call__(self, *args, **kwargs):
        return self.__func__(*args, **kwargs)
