IMAGE := alpine:latest
PACKAGES := moreutils


build: ARTIFACTS=dist 
build: export PYAZ_VERSION=$(shell git describe --dirty --abbrev=6 --always)
build:
	envsubst < setup.cfg | sponge setup.cfg
	python3 setup.py sdist


.PHONY: test
test:
	cd src && python3 -m unittest discover -p '*.py' -s ../test/

